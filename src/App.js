import React from 'react'
import moment from 'moment'
import api from './configs/api'
import firebase from './configs/firebase'

const App = () => (
  <div>
    {moment().format()}
    <br />
    {api.APP_BASE_URL}
    <br />
    {firebase.FIREBASE_KEY}
    Test
  </div>
)
export default App
