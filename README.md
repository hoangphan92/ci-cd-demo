## RULE CODE

✅ OK

```
import { Add as AddIcon } from '@material-ui/icons';
import { Tabs } from '@material-ui/core';
// ^^^^ 1st or top-level
```

✅ OK

```
import AddIcon from '@material-ui/icons/Add';
import Tabs from '@material-ui/core/Tabs';
// ^^^^ 2nd level
```

❌ NOT OK

```
import TabIndicator from '@material-ui/core/Tabs/TabIndicator';
// 3rd level
```
